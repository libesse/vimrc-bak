"
" ~/.vimrc
"
" Last full review: 2020-02-23
"
"======================================================================
" vim: set ts=4 sw=4 tw=78 noet :

" 方便的变量
let g:is_win = has('win32') || has('win64')
let g:is_linux = has('unix') && !has('macunix')
let g:is_mac = has('macunix')

" General settings
" ----------------
" Read documentation about each option by executing :h <option>
set nocompatible                  " do not preserve compatibility with Vi
set backspace=indent,eol,start    " backspace key should delete indentation, line ends, characters
set autoindent                    " indent automatically (useful for formatoptions)
set cindent                       " 打开 C/C++ 语言优化
set nowrap                        " 关闭自动换行
set ttimeout                      " 打开功能键超时检测（终端下功能键为一串 ESC 开头的字符串）
set ttimeoutlen=50                " 功能键超时检测 50 毫秒
set ruler                         " 在状态栏上显示光标位置

"----------------------------------------------------------------------
" 默认缩进模式（可以后期覆盖）
"----------------------------------------------------------------------
set expandtab                     " use spaces instead of tabs
set tabstop=4                     " tab character width
set shiftwidth=4                  " needs to be the same as tabstop
set softtabstop=4                 " 设置 expandtab 那么展开 tab 为多少字符

"----------------------------------------------------------------------
" 搜索设置
"----------------------------------------------------------------------
set ignorecase                    " 搜索时忽略大小写
set smartcase                     " ignore case if the search contains majuscules
set hlsearch                      " highlight all matches of last search
set incsearch                     " enable incremental searching (get feedback as you type)

"----------------------------------------------------------------------
" 编码设置
"----------------------------------------------------------------------
if has('multi_byte')
    " 内部工作编码
    set encoding=utf-8

    " 文件默认编码
    set fileencoding=utf-8

    " 打开文件时自动尝试下面顺序的编码
    set fileencodings=ucs-bom,utf-8,gbk,gb18030,big5,euc-jp,latin1
endif

"----------------------------------------------------------------------
" 允许 Vim 自带脚本根据文件类型自动设置缩进等
"----------------------------------------------------------------------
if has('autocmd')
    filetype plugin indent on
endif

"----------------------------------------------------------------------
" 语法高亮设置
"----------------------------------------------------------------------
if has('syntax')
    syntax enable
    syntax on
endif

"----------------------------------------------------------------------
" 其他设置
"----------------------------------------------------------------------
set showmatch                     " 显示匹配的括号
set autoread                      " 自动读取因外部改变的文件
set matchtime=2                   " 减少显示括号匹配的时间
set display=lastline              " 显示最后一行
set wildmenu                      " enable tab completion with suggestions when executing commands
set wildmode=list:longest,full    " settings for how to complete matched strings
set lazyredraw                    " 延迟绘制（提示性能）
set autoread                      " detect when a file has been modified externally
"set spelllang=en                  " languages to check for spelling (english)
"set spellsuggest=10               " number of suggestions for correct spelling
set nostartofline                 " keep cursor in the same column when moving between lines
set hidden                        " hide buffers
set clipboard=unnamedplus

" 错误格式
"set errorformat+=[%f:%l]\ ->\ %m,[%f:%l]:%m

" invisible characters to display (with :set list)
set listchars=tab:›\ ,nbsp:␣,trail:•,precedes:«,extends:»

" 设置 tags：当前文件所在目录往上向根目录搜索直到碰到 .tags 文件
" 或者 Vim 当前目录包含 .tags 文件
set tags=./.tags;,.tags

" defines how automatic formatting should be done (see :h fo-table)
set formatoptions-=t formatoptions-=o formatoptions+=crqjnl1
set formatoptions+=B              " 合并两行中文时，不在中间加空格

" 设置分隔符可视
set listchars=tab:›\ ,space:·,nbsp:␣,trail:•,eol:¬,precedes:«,extends:»

"----------------------------------------------------------------------
" 显示设置
"----------------------------------------------------------------------
set laststatus=2
set number
set signcolumn=yes
set showtabline=2
set list
set showcmd                       " show command in last line of the screen
" set showmode
set splitright

" Theme settings
" --------------

" Italic options should be put before colorscheme setting,
" see https://goo.gl/8nXhcp
if has("termguicolors")     " set true colors
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

highlight Normal ctermbg=black ctermfg=white
set background=dark
let g:gruvbox_italics=1
let g:gruvbox_italicize_strings=1
let g:gruvbox_filetype_hi_groups = 0
let g:gruvbox_plugin_hi_groups = 0
colorscheme gruvbox8_hard

"----------------------------------------------------------------------
" GVim 界面设置
"----------------------------------------------------------------------
if has('gui_running')
    "set lines=35 columns=80
    set winaltkeys=no             " Windows 禁用 ALT 操作菜单（使得 ALT 可以用到 Vim 里）
    set guioptions-=T
    set guioptions-=m
    set guioptions-=L
    set guioptions-=r
    set guioptions-=R
    set guioptions-=e " 使用内置 tab 样式而不是 gui

    if g:is_linux
        set guifontwide=Source\ Han\ Sans\ 12
        set guifont=DejaVu\ Sans\ Mono\ 12
    else
        "set guifont=Source\ Han\ Sans:h12
        set guifont=DejaVu_Sans_Mono:h12
    endif

    " 在 GUI 中禁用 bells
    set vb t_vb=

    " keymap to switch table in gui
    " http://www.skywind.me/blog/archives/1690
    noremap <silent><c-tab> :tabprev<CR>
    inoremap <silent><c-tab> <ESC>:tabprev<CR>
    noremap <silent><m-1> :tabn 1<cr>
    noremap <silent><m-2> :tabn 2<cr>
    noremap <silent><m-3> :tabn 3<cr>
    noremap <silent><m-4> :tabn 4<cr>
    noremap <silent><m-5> :tabn 5<cr>
    noremap <silent><m-6> :tabn 6<cr>
    noremap <silent><m-7> :tabn 7<cr>
    noremap <silent><m-8> :tabn 8<cr>
    noremap <silent><m-9> :tabn 9<cr>
    noremap <silent><m-0> :tabn 10<cr>
    inoremap <silent><m-1> <ESC>:tabn 1<cr>
    inoremap <silent><m-2> <ESC>:tabn 2<cr>
    inoremap <silent><m-3> <ESC>:tabn 3<cr>
    inoremap <silent><m-4> <ESC>:tabn 4<cr>
    inoremap <silent><m-5> <ESC>:tabn 5<cr>
    inoremap <silent><m-6> <ESC>:tabn 6<cr>
    inoremap <silent><m-7> <ESC>:tabn 7<cr>
    inoremap <silent><m-8> <ESC>:tabn 8<cr>
    inoremap <silent><m-9> <ESC>:tabn 9<cr>
    inoremap <silent><m-0> <ESC>:tabn 10<cr>

    function! s:Filter_Push(desc, wildcard)
    let g:browsefilter .= a:desc . " (" . a:wildcard . ")\t" . a:wildcard . "\n"
    endfunc

    let g:browsefilter = ''
    call s:Filter_Push("All Files", "*")
    call s:Filter_Push("C/C++/Object-C", "*.c;*.cpp;*.cc;*.h;*.hh;*.hpp;*.m;*.mm")
    call s:Filter_Push("Python", "*.py;*.pyw")
    call s:Filter_Push("Text", "*.txt")
    call s:Filter_Push("Vim Script", "*.vim")

function! Open_Browse(where)
    let l:path = expand("%:p:h")
    if l:path == '' | let l:path = getcwd() | endif
    if exists('g:browsefilter') && exists('b:browsefilter')
    if g:browsefilter != ''
    let b:browsefilter = g:browsefilter
    endif
    endif
    if a:where == 0
    exec 'browse e '.fnameescape(l:path)
    elseif a:where == 1
    exec 'browse vnew '.fnameescape(l:path)
    else
    exec 'browse tabnew '.fnameescape(l:path)
    endif
    endfunc

    noremap <silent><m-o> :call Open_Browse(2)<CR>
    inoremap <silent><m-o> <ESC>:call Open_Browse(2)<CR>
endif

"----------------------------------------------------------------------
" 状态栏设置
"----------------------------------------------------------------------
set statusline=                                 " 清空状态了
set statusline+=\ %F                            " 文件名
set statusline+=\ [%1*%M%*%n%R%H]               " buffer 编号和状态
set statusline+=%=                              " 向右对齐
set statusline+=\ %y                            " 文件类型

" 最右边显示文件编码和行号等信息，并且固定在一个 group 中，优先占位
set statusline+=\ %0(%{&fileformat}\ [%{(&fenc==\"\"?&enc:&fenc).(&bomb?\",BOM\":\"\")}]\ %v:%l/%L%)

"----------------------------------------------------------------------
" 更改样式
"----------------------------------------------------------------------

" 更清晰的错误标注：默认一片红色背景，语法高亮都被搞没了
" 只显示红色或者蓝色下划线或者波浪线
hi! clear SpellBad
hi! clear SpellCap
hi! clear SpellRare
hi! clear SpellLocal
if has('gui_running')
    hi! SpellBad gui=undercurl guisp=red
    hi! SpellCap gui=undercurl guisp=blue
    hi! SpellRare gui=undercurl guisp=magenta
    hi! SpellRare gui=undercurl guisp=cyan
else
    hi! SpellBad term=standout ctermfg=1 term=underline cterm=underline
    hi! SpellCap term=underline cterm=underline
    hi! SpellRare term=underline cterm=underline
    hi! SpellLocal term=underline cterm=underline
endif

" 去掉 sign column 的白色背景
hi! SignColumn guibg=NONE ctermbg=NONE

" 修改行号为浅灰色，默认主题的黄色行号很难看，换主题可以仿照修改
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE 
    \ gui=NONE guifg=DarkGrey guibg=NONE

" 修正补全目录的色彩：默认太难看
hi! Pmenu guibg=gray guifg=black ctermbg=gray ctermfg=black
hi! PmenuSel guibg=gray guifg=brown ctermbg=brown ctermfg=gray

"----------------------------------------------------------------------
" 终端设置，隐藏行号和侧边栏
"----------------------------------------------------------------------
if has('terminal') && exists(':terminal') == 2
    if exists('##TerminalOpen')
        augroup VimUnixTerminalGroup
            au! 
            au TerminalOpen * setlocal nonumber signcolumn=no
        augroup END
    endif
endif

"----------------------------------------------------------------------
" quickfix 设置，隐藏行号
"----------------------------------------------------------------------
augroup VimInitStyle
    au!
    au FileType qf setlocal nonumber
augroup END

"----------------------------------------------------------------------
" 备份设置
"----------------------------------------------------------------------
" Put all temporary files under the same directory.
" https://github.com/mhinz/vim-galore#handling-backup-swap-undo-and-viminfo-files
set nobackup
"set backupdir   =$HOME/.vim/files/backup/
set backupext   =-vimbackup
set backupskip  =

set noswapfile
"set directory   =$HOME/.vim/files/swap/
"set updatecount =100

set noundofile
"set undodir     =$HOME/.vim/files/undo/

set viminfo     ='100,n$HOME/.vim/files/info/viminfo

"----------------------------------------------------------------------
" 文件类型微调
"----------------------------------------------------------------------
augroup InitFileTypesGroup

    " 清除同组的历史 autocommand
    au!

    " markdown 允许自动换行
    au FileType markdown setlocal wrap

    " rst
    " https://github.com/gu-fan/riv.vim/issues/144
    " or disable Cursor Highlighting by set 'g:riv_link_cursor_hl_' to 0
    au FileType rst setlocal wrap maxmempattern=2000
augroup END
